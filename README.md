# Classroom
---

## Description

Classroom management.

## Installation

After cloning the project :
```
$ npm install
```

Starting the dev server :
```
$ npm start
```
