import React from 'react'
import { Router, Route, IndexRedirect, browserHistory } from 'react-router'

import App from '../containers/App'
import Home from '../containers/Home'
import Users from '../containers/Users'
import Classrooms from '../containers/Classrooms'
import UserDetails from '../containers/UserDetails'
import UserEdit from '../containers/UserEdit'
import UserCreate from '../containers/UserCreate'

export default () =>
  <Router history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRedirect to='/home' />
      <Route path='/home' component={Home} />
      <Route path='/users' component={Users} />
      <Route path='/users/new' component={UserCreate} />
      <Route path='/users/:id' component={UserDetails} />
      <Route path='/users/:id/edit' component={UserEdit} />
      <Route path='/classes' component={Classrooms} />
    </Route>
  </Router>
