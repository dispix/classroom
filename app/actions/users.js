import fetch from 'isomorphic-fetch'

/**
 *  Actions types
 *  @type  {String}
 */
export const USERS_FETCH_START = 'USERS_FETCH_START'
export const USERS_FETCH_RESOLVE = 'USERS_FETCH_RESOLVE'
export const USERS_FETCH_REJECT = 'USERS_FETCH_REJECT'

/**
 *  Action dispatched when a fetch is initiated
 *  @method  fetchStart
 *  @return  {Object}    Redux action
 */
export const fetchStart = () => ({
  type: USERS_FETCH_START
})

/**
 *  Action dispatched when a fetch is succesful
 *  @method  fetchResolve
 *  @param   {Object}      response  The response object from the server fetch
 *  @return  {Object}                Redux action
 */
export const fetchResolve = response => ({
  type: USERS_FETCH_RESOLVE,
  response
})

/**
 *  Action dispatched when a fetch timed-out or was rejected by the server
 *  @method  updateUser
 *  @return  {Object}    Redux action
 */
export const fetchReject = () => ({
  type: USERS_FETCH_REJECT
})

/**
 *  Async action that try to fetch users
 *  @method  fetchUsers
 *  @return  {function}    The async action that will be dispatched when the promise is settled
 */
export const fetchUsers = () => dispatch => {
  dispatch(fetchStart())

  return fetch('http://localhost:3000/users')
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      return res.json()
    })
    .then(json => dispatch(fetchResolve(json)))
    .catch(err => dispatch(fetchReject(err)))
}
