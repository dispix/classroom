import fetch from 'isomorphic-fetch'

/**
 *  Actions types
 *  @type  {String}
 */
export const USER_DETAILS_FETCH_START = 'USER_DETAILS_FETCH_START'
export const USER_DETAILS_FETCH_RESOLVE = 'USER_DETAILS_FETCH_RESOLVE'
export const USER_DETAILS_FETCH_REJECT = 'USER_DETAILS_FETCH_REJECT'
export const USER_CLASSROOMS_FETCH_START = 'USER_CLASSROOMS_FETCH_START'
export const USER_CLASSROOMS_FETCH_RESOLVE = 'USER_CLASSROOMS_FETCH_RESOLVE'
export const USER_CLASSROOMS_FETCH_REJECT = 'USER_CLASSROOMS_FETCH_REJECT'

/**
 *  Action dispatched when a fetch is initiated
 *  @method  fetchStart
 *  @return  {Object}    Redux action
 */
export const fetchStart = type => ({
  type
})

/**
 *  Action dispatched when a fetch is succesful
 *  @method  fetchResolve
 *  @param   {Object}      response  The response object from the server fetch
 *  @return  {Object}                Redux action
 */
export const fetchResolve = (type, response) => ({
  type,
  response
})

/**
 *  Action dispatched when a fetch timed-out or was rejected by the server
 *  @method  updateUser
 *  @return  {Object}    Redux action
 */
export const fetchReject = type => ({
  type
})

/**
 *  Async action that try to fetch user details
 *  @method  fetchUserDetails
 *  @param   {String}      id   The user that will be fetched
 *  @return  {function}         The async action that will be dispatched when the promise is settled
 */
export const fetchUserDetails = id => dispatch => {
  dispatch(fetchStart(USER_DETAILS_FETCH_START))

  return fetch(`http://localhost:3000/users/${id}`)
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      return res.json()
    })
    .then(json => dispatch(fetchResolve(USER_DETAILS_FETCH_RESOLVE, json)))
    .catch(err => dispatch(fetchReject(USER_DETAILS_FETCH_REJECT, err)))
}

/**
 *  Async action that try to fetch classrooms related to a user
 *  @method  fetchUserClassrooms
 *  @param   {String}      id   The user that we search for
 *  @return  {function}         The async action that will be dispatched when the promise is settled
 */
export const fetchUserClassrooms = id => dispatch => {
  dispatch(fetchStart(USER_CLASSROOMS_FETCH_START))

  return fetch(`http://localhost:3000/classrooms?users_like=${id}`)
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      return res.json()
    })
    .then(json => dispatch(fetchResolve(USER_CLASSROOMS_FETCH_RESOLVE, json)))
    .catch(err => dispatch(fetchReject(USER_CLASSROOMS_FETCH_REJECT, err)))
}
