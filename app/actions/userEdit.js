import fetch from 'isomorphic-fetch'

/**
 *  Actions types
 *  @type  {String}
 */
export const RESET_EDIT = 'RESET_EDIT'
export const USER_EDIT_FETCH_START = 'USER_EDIT_FETCH_START'
export const USER_EDIT_FETCH_RESOLVE = 'USER_EDIT_FETCH_RESOLVE'
export const USER_EDIT_FETCH_REJECT = 'USER_EDIT_FETCH_REJECT'

/**
 *  Fetch methods
 *  @type  {String}
 */
const FETCH_POST = 'POST'
const FETCH_PATCH = 'PATCH'
const FETCH_DELETE = 'DELETE'

/**
 *  Reset the store state
 *  @method             resetEditState
 *  @return {Object}    Redux action
 */
export const resetEditState = () => ({
  type: RESET_EDIT
})

/**
 *  Action dispatched when a fetch is initiated
 *  @method  fetchStart
 *  @return  {Object}    Redux action
 */
export const fetchStart = method => ({
  type: USER_EDIT_FETCH_START,
  method
})

/**
 *  Action dispatched when a fetch is succesful
 *  @method  fetchResolve
 *  @param   {Object}      response  The response object from the server fetch
 *  @return  {Object}                Redux action
 */
export const fetchResolve = response => ({
  type: USER_EDIT_FETCH_RESOLVE,
  response
})

/**
 *  Action dispatched when a fetch timed-out or was rejected by the server
 *  @method  updateUser
 *  @return  {Object}    Redux action
 */
export const fetchReject = () => ({
  type: USER_EDIT_FETCH_REJECT
})

/**
 *  Async action that try to create a new user
 *  @method  createUser
 *  @param   {Object}      user   The user that will be fetched
 *  @return  {Function}           The async action that will be dispatched when the promise is settled
 */
export const createUser = user => dispatch => {
  dispatch(fetchStart(FETCH_POST))

  return fetch(`http://localhost:3000/users`,
    {
      method: FETCH_POST,
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({ ...user, ip_adress: '127.0.0.1' })
    })
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      return res.json()
    })
    .then(json => dispatch(fetchResolve({ ...json, created: true })))
    .catch(err => dispatch(fetchReject(err)))
}

/**
 *  Async action that try to edit an existing user
 *  @method  editUser
 *  @param   {Object}  user   The user to edit with its new attributes
 *  @return  {Function}       The async action that will be dispatched when the promise is settled
 */
export const updateUser = user => dispatch => {
  dispatch(fetchStart(FETCH_PATCH))

  return fetch(`http://localhost:3000/users/${user.id}`,
    {
      method: FETCH_PATCH,
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify(user)
    })
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      console.log('editUser res: ', res)
      return res.json()
    })
    .then(json => dispatch(fetchResolve({...json, updated: true})))
    .catch(err => dispatch(fetchReject(err)))
}

/**
 *  Async action that try to delete an existing user
 *  @method  deleteUser
 *  @param   {String}    id  The id of the user to delete
 *  @return  {Function}       The async action that will be dispatched when the promise is settled
 */
export const deleteUser = id => dispatch => {
  dispatch(fetchStart(FETCH_DELETE))

  return fetch(`http://localhost:3000/users/${id}`,
    {
      method: FETCH_DELETE
    })
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      return res.json()
    })
    .then(json => dispatch(fetchResolve({deleted: true})))
    .catch(err => dispatch(fetchReject(err)))
}
