import fetch from 'isomorphic-fetch'

/**
 *  Actions types
 *  @type  {String}
 */
export const CLASSROOMS_FETCH_START = 'CLASSROOMS_FETCH_START'
export const CLASSROOMS_FETCH_RESOLVE = 'CLASSROOMS_FETCH_RESOLVE'
export const CLASSROOMS_FETCH_REJECT = 'CLASSROOMS_FETCH_REJECT'

/**
 *  Action dispatched when a fetch is initiated
 *  @method  fetchStart
 *  @return  {Object}    Redux action
 */
export const fetchStart = () => ({
  type: CLASSROOMS_FETCH_START
})

/**
 *  Action dispatched when a fetch is succesful
 *  @method  fetchResolve
 *  @param   {Object}      response  The response object from the server fetch
 *  @return  {Object}                Redux action
 */
export const fetchResolve = (response) => ({
  type: CLASSROOMS_FETCH_RESOLVE,
  response
})

/**
 *  Action dispatched when a fetch timed-out or was rejected by the server
 *  @method  updateUser
 *  @return  {Object}    Redux action
 */
export const fetchReject = () => ({
  type: CLASSROOMS_FETCH_REJECT
})

/**
 *  Async action that try to fetch users
 *  @method  fetchUsers
 *  @return  {function}    The async action that will be dispatched when the promise is settled
 */
export const fetchClassrooms = () => dispatch => {
  dispatch(fetchStart())

  return fetch('http://localhost:3000/classrooms')
    .then(res => {
      if (!res.ok) throw Error(res.statusText)
      return res.json()
    })
    .then(json => dispatch(fetchResolve(json)))
    .catch(err => dispatch(fetchReject(err)))
}
