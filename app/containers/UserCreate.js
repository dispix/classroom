import React from 'react'
import { connect } from 'react-redux'

import NewUserForm from '../components/userForm/NewUserForm'
import { resetEditState, createUser } from '../actions/userEdit'

/**
 * The `UserCreate` class is used to fetch and display details about a specific user
 */
class UserCreate extends React.Component {
  static get propTypes () {
    return {
      userEdit: React.PropTypes.object.isRequired,
      createUser: React.PropTypes.func.isRequired,
      resetEditState: React.PropTypes.func.isRequired
    }
  }

  componentDidMount () {
    console.log('cpdidm')
    this.props.resetEditState()
  }

  render () {
    return (
      <div className='user-edit'>
        <NewUserForm
          editState={this.props.userEdit}
          createUser={this.props.createUser}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({ userEdit: state.userEdit })
const mapDispatchToProps = dispatch => (
  {
    resetEditState: () => dispatch(resetEditState()),
    createUser: user => dispatch(createUser(user))
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(UserCreate)
