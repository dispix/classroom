import React from 'react'
import { Link } from 'react-router'

import Sidebar from '../components/sidebar/Sidebar'

import '../main.scss'

/**
 * The `App` class is the main class of the application
 */
class App extends React.Component {
  toggleMenu () {
    this.refs['app-menu'].toggle()
  }

  render () {
    const activeStyle = { fontWeight: 'bold' }

    return (
      <div className='app-container'>
        <Sidebar
          classes='app-menu'
          ref='app-menu'
        >
          <ul>
            <li onClick={() => this.toggleMenu()}>
              <Link to='/home' activeStyle={activeStyle}>
                Home
              </Link>
            </li>
            <li onClick={() => this.toggleMenu()}>
              <Link to='/users' activeStyle={activeStyle}>
                Students
              </Link>
            </li>
            <li onClick={() => this.toggleMenu()}>
              <Link to='/classes' activeStyle={activeStyle}>
                Classes
              </Link>
            </li>
          </ul>
        </Sidebar>

        <div className='main'>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default App
