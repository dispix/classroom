import React from 'react'
import { connect } from 'react-redux'

import { fetchClassrooms } from '../actions/classrooms'
import ClassroomTile from '../components/classroomTile/ClassroomTile'
import Loader from '../components/loader/Loader'

/**
 * The `Classrooms` class is used to fetch and display lists of classes
 */
class Classrooms extends React.Component {
  static get propTypes () {
    return {
      isFetching: React.PropTypes.bool.isRequired,
      fetchClassrooms: React.PropTypes.func.isRequired,
      classrooms: React.PropTypes.array.isRequired
    }
  }

  componentDidMount () {
    this.props.fetchClassrooms()
  }

  render () {
    return (
      <div className='classrooms-container'>
        <h1>Classes</h1>
        { this.props.isFetching && <Loader /> }
        { !this.props.isFetching &&
          <div className='classroom-list'>
            {
              this.props.classrooms.map(classroom =>
                <ClassroomTile
                  key={classroom.id}
                  classroom={classroom}
                />
              )
            }
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = state => state.classrooms
const mapDispatchToProps = dispatch => (
  {
    fetchClassrooms: () => dispatch(fetchClassrooms())
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(Classrooms)
