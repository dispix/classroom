import React from 'react'
import { connect } from 'react-redux'

import UserInfos from '../components/userInfos/UserInfos'
import { fetchUserDetails, fetchUserClassrooms } from '../actions/userDetails'
import Loader from '../components/loader/Loader'

/**
 * The `UserDetails` class is used to fetch and display details about a specific user
 */
class UserDetails extends React.Component {
  static get propTypes () {
    return {
      isFetching: React.PropTypes.bool.isRequired,
      fetchUserDetails: React.PropTypes.func.isRequired,
      fetchUserClassrooms: React.PropTypes.func.isRequired,
      userDetails: React.PropTypes.object
    }
  }

  componentDidMount () {
    this.props.fetchUserDetails(this.props.params.id)
    this.props.fetchUserClassrooms(this.props.params.id)
  }

  render () {
    return (
      <div className='user-details'>
        { this.props.isFetching &&
          <Loader />
        }
        { !this.props.isFetching &&
          <UserInfos
            user={this.props.user}
            isFetchingClassrooms={this.props.isFetchingClassrooms}
          />
        }
      </div>
    )
  }
}

const mapStateToProps = state => state.userDetails
const mapDispatchToProps = dispatch => (
  {
    fetchUserDetails: id => dispatch(fetchUserDetails(id)),
    fetchUserClassrooms: id => dispatch(fetchUserClassrooms(id))
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails)
