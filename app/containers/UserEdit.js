import React from 'react'
import { connect } from 'react-redux'

import UserForm from '../components/userForm/UserForm'
import { fetchUserDetails } from '../actions/userDetails'
import { resetEditState, updateUser, deleteUser } from '../actions/userEdit'
import Loader from '../components/loader/Loader'

/**
 * The `UserEdit` class is used to fetch and display details about a specific user
 */
class UserEdit extends React.Component {
  static get propTypes () {
    return {
      userDetails: React.PropTypes.object.isRequired,
      userEdit: React.PropTypes.object.isRequired,
      updateUser: React.PropTypes.func.isRequired,
      deleteUser: React.PropTypes.func.isRequired,
      fetchUserDetails: React.PropTypes.func.isRequired,
      resetEditState: React.PropTypes.func.isRequired
    }
  }

  componentDidMount () {
    this.props.resetEditState()
    this.props.fetchUserDetails(this.props.params.id)
  }

  render () {
    return (
      <div className='user-edit'>
        { this.props.userDetails.isFetching &&
          <Loader />
        }
        { !this.props.userDetails.isFetching &&
          <UserForm
            user={this.props.userDetails.user}
            editState={this.props.userEdit}
            updateUser={this.props.updateUser}
            deleteUser={this.props.deleteUser}
          />
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({userDetails: state.userDetails, userEdit: state.userEdit})
const mapDispatchToProps = dispatch => (
  {
    resetEditState: () => dispatch(resetEditState()),
    fetchUserDetails: id => dispatch(fetchUserDetails(id)),
    updateUser: user => dispatch(updateUser(user)),
    deleteUser: id => dispatch(deleteUser(id))
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(UserEdit)
