import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import { fetchUsers } from '../actions/users'
import UserTile from '../components/userTile/UserTile'
import Loader from '../components/loader/Loader'

/**
 * The `Users` class is used to fetch and display lists of registered users
 */
class Users extends React.Component {
  static get propTypes () {
    return {
      isFetching: React.PropTypes.bool.isRequired,
      fetchUsers: React.PropTypes.func.isRequired,
      users: React.PropTypes.array.isRequired
    }
  }

  constructor () {
    super()
    this.state = {
      users: []
    }
  }

  componentDidMount () {
    this.props.fetchUsers()
  }

  render () {
    return (
      <div className='users-container'>
        <h1>Students</h1>
        { this.props.isFetching && <Loader /> }
        { !this.props.isFetching &&
          <div className='user-list'>
            {
              this.props.users.map(user =>
                <UserTile
                  key={user.id}
                  user={user}
                />
              )
            }
            <Link to={`/users/new`} className='add-user-tile'>
              <h3>Add new student</h3>
            </Link>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = state => state.users
const mapDispatchToProps = dispatch => (
  {
    fetchUsers: () => dispatch(fetchUsers())
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(Users)
