import userInitialState from './userInitialState'
import { USERS_FETCH_START, USERS_FETCH_RESOLVE, USERS_FETCH_REJECT } from '../../actions/users'

/**
 *  User (student) reducer for Redux
 *  @method  userReducer
 *  @param   {Object}     [state=userInitialState]  The current sub-store state
 *  @param   {Object}     action                    The action to be dispatched
 *  @return  {Object}                               The new state of the redux sub-store
 */
function userReducer (state = userInitialState, action) {
  switch (action.type) {
    case USERS_FETCH_START:
      return { ...state, isFetching: true }
    case USERS_FETCH_RESOLVE:
      return {
        isFetching: false,
        isReject: false,
        users: [...action.response]
      }
    case USERS_FETCH_REJECT:
      return {
        ...state,
        isFetching: false,
        isReject: true
      }
    default:
      return state
  }
}

export default userReducer
