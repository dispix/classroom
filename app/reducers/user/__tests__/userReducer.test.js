import faker from 'faker'

import userReducer from '../userReducer'
import * as userActions from '../userActions'
import userInitialState from '../userInitialState'

describe('userReducer', () => {
  it('can add users', () => {
    const action = {
      type: userActions.ADD_USER,
      payload: {
        id: faker.random.uuid(),
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        birthday: faker.date.past(),
        email: faker.internet.email()
      }
    }

    let expected = {}
    expected[action.payload.id] = action.payload

    expect(userReducer(userInitialState, action)).toEqual(expected)
  })
})
