import userDetailsInitialState from './userDetailsInitialState'
import {
  USER_DETAILS_FETCH_START,
  USER_DETAILS_FETCH_RESOLVE,
  USER_DETAILS_FETCH_REJECT,
  USER_CLASSROOMS_FETCH_START,
  USER_CLASSROOMS_FETCH_RESOLVE,
  USER_CLASSROOMS_FETCH_REJECT
} from '../../actions/userDetails'

/**
 *  User (student) details reducer for Redux
 *  @method  userDetailsReducer
 *  @param   {Object}     [state=userInitialState]  The current sub-store state
 *  @param   {Object}     action                    The action to be dispatched
 *  @return  {Object}                               The new state of the redux sub-store
 */
function userDetailsReducer (state = userDetailsInitialState, action) {
  switch (action.type) {
    case USER_DETAILS_FETCH_START:
      return { ...state, isFetching: true }
    case USER_DETAILS_FETCH_RESOLVE:
      return {
        ...state,
        isFetching: false,
        isReject: false,
        user: {
          ...action.response,
          classrooms: state.user.classrooms
        }
      }
    case USER_DETAILS_FETCH_REJECT:
      return {
        ...state,
        isFetching: false,
        isReject: true
      }
    case USER_CLASSROOMS_FETCH_START:
      return { ...state, isFetchingClassrooms: true }
    case USER_CLASSROOMS_FETCH_RESOLVE:
      return {
        ...state,
        isFetchingClassrooms: false,
        isRejectClassrooms: false,
        user: {...state.user, classrooms: action.response}
      }
    case USER_CLASSROOMS_FETCH_REJECT:
      return {
        ...state,
        isFetchingClassrooms: false,
        isRejectClassrooms: true
      }
    default:
      return state
  }
}

export default userDetailsReducer
