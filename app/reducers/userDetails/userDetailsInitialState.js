export default {
  isFetching: false,
  isReject: false,
  isFetchingClassrooms: false,
  isRejectClassrooms: false,
  user: {
    classrooms: []
  }
}
