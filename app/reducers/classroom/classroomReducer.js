import classroomInitialState from './classroomInitialState'
import { CLASSROOMS_FETCH_START, CLASSROOMS_FETCH_RESOLVE, CLASSROOMS_FETCH_REJECT } from '../../actions/classrooms'

/**
 *  Classrooms reducer for Redux
 *  @method  classroomReducer
 *  @param   {Object}     [state=classroomInitialState]   The current sub-store state
 *  @param   {Object}     action                          The action to be dispatched
 *  @return  {Object}                                     The new state of the redux sub-store
 */
function classroomReducer (state = classroomInitialState, action) {
  switch (action.type) {
    case CLASSROOMS_FETCH_START:
      return { ...state, isFetching: true }
    case CLASSROOMS_FETCH_RESOLVE:
      return {
        isFetching: false,
        isReject: false,
        classrooms: [...action.response]
      }
    case CLASSROOMS_FETCH_REJECT:
      return {
        ...state,
        isFetching: false,
        isReject: true
      }
    default:
      return state
  }
}

export default classroomReducer
