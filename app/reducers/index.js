import { combineReducers } from 'redux'

import users from './user/userReducer'
import userDetails from './userDetails/userDetailsReducer'
import userEdit from './userEdit/userEditReducer'
import classrooms from './classroom/classroomReducer'

export default combineReducers({ users, userDetails, userEdit, classrooms })
