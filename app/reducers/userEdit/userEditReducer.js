import userEditInitialState from './userEditInitialState'
import { RESET_EDIT, USER_EDIT_FETCH_START, USER_EDIT_FETCH_RESOLVE, USER_EDIT_FETCH_REJECT } from '../../actions/userEdit'

/**
 *  User (student) edition reducer for Redux
 *  @method  userEditReducer
 *  @param   {Object}     [state=userInitialState]  The current sub-store state
 *  @param   {Object}     action                    The action to be dispatched
 *  @return  {Object}                               The new state of the redux sub-store
 */
function userEditReducer (state = userEditInitialState, action) {
  switch (action.type) {
    case RESET_EDIT:
      return userEditInitialState
    case USER_EDIT_FETCH_START:
      return {
        ...userEditInitialState,
        isFetching: true,
        method: action.method
      }
    case USER_EDIT_FETCH_RESOLVE:
      return {
        isFetching: false,
        isReject: false,
        method: '',
        response: {
          ...action.response,
          deleted: state.method === 'DELETE',
          created: state.method === 'POST'
        }
      }
    case USER_EDIT_FETCH_REJECT:
      return {
        isFetching: false,
        isReject: true,
        method: '',
        response: {}
      }
    default:
      return state
  }
}

export default userEditReducer
