import React from 'react'

/**
 *  Button component
 *  @function  Button
 *  @param    {String}    label      Inner text of the button
 *  @param    {String}    className  Classes for the button, will be appended to the `button` class
 *  @param    {Function}  onClick    Callback triggered when the user clicks the button
 */
const Button = ({children, classes, onClick}) =>
  <a
    className={`button ${classes}`}
    onClick={onClick}
  >
    {children}
  </a>

Button.propTypes = {
  classes: React.PropTypes.string,
  onClick: React.PropTypes.func.isRequired
}

export default Button
