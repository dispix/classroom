import React from 'react'
import { Link } from 'react-router'

import Loader from '../loader/Loader'

import './UserForm.scss'

class UserForm extends React.Component {
  static get propTypes () {
    return {
      user: React.PropTypes.object.isRequired,
      updateUser: React.PropTypes.func.isRequired,
      deleteUser: React.PropTypes.func.isRequired
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user
    }
  }

  onChange (event) {
    const partialState = { user: this.state.user }
    partialState.user[event.target.id] = event.target.value

    this.setState({...partialState})
  }

  render () {
    return (
      <div>
        <h2>Student edition</h2>

        {this.props.editState.response.deleted &&
          <div className='confirm-delete'>
            <p>Done. This student has been deleted.</p>
            <Link to='/users'>Back</Link>
          </div>
        }

        {!this.props.editState.response.deleted &&
          <div>
            <form
              className='user-form'
              action={`/users/${this.props.user.id}/edit`}
              method='PATCH'
              >
              <div className='form-group'>
                <label htmlFor='first_name'>Name</label>
                <input type='text' id='first_name' value={this.state.user.first_name} onChange={e => this.onChange(e)} />
              </div>
              <div className='form-group'>
                <label htmlFor='last_name'>Surname</label>
                <input type='text' id='last_name' value={this.state.user.last_name} onChange={e => this.onChange(e)} />
              </div>
              <div className='form-group'>
                <label htmlFor='email'>E-mail</label>
                <input type='text' id='email' value={this.state.user.email} onChange={e => this.onChange(e)} />
              </div>
            </form>
            <div className='submit'>
              <a
                className='edit-submit'
                onClick={this.props.editState.isFetching ? null : () => this.props.updateUser(this.state.user)}
                >
                {this.props.editState.isFetching ? <Loader /> : 'Update'}
              </a>

              <a
                className='delete-submit'
                onClick={this.props.editState.isFetching ? null : () => this.props.deleteUser(this.state.user.id)}
                >
                {this.props.editState.isFetching ? <Loader /> : 'Delete'}
              </a>
            </div>
          </div>
        }

      </div>
    )
  }
}

export default UserForm
