import React from 'react'
import { Link } from 'react-router'

import Loader from '../loader/Loader'

import './UserForm.scss'

class NewUserForm extends React.Component {
  static get propTypes () {
    return {
      editState: React.PropTypes.object.isRequired,
      createUser: React.PropTypes.func.isRequired
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      user: {
        first_name: '',
        last_name: '',
        email: ''
      }
    }
  }

  onChange (event) {
    const partialState = { user: this.state.user }
    partialState.user[event.target.id] = event.target.value

    this.setState({...partialState})
  }

  render () {
    return (
      <div>
        <h2>New student</h2>

        {this.props.editState.response.created &&
          <div className='confirm-create'>
            <p>Success! The student has been created.</p>
            <Link to='/users'>Back</Link>
          </div>
        }

        {!this.props.editState.response.created &&
          <div>
            <form
              className='user-form'
              action='/users/new'
              method='POST'
              >
              <div className='form-group'>
                <label htmlFor='first_name'>Name</label>
                <input type='text' id='first_name' value={this.state.user.first_name} onChange={e => this.onChange(e)} />
              </div>
              <div className='form-group'>
                <label htmlFor='last_name'>Surname</label>
                <input type='text' id='last_name' value={this.state.user.last_name} onChange={e => this.onChange(e)} />
              </div>
              <div className='form-group'>
                <label htmlFor='email'>E-mail</label>
                <input type='text' id='email' value={this.state.user.email} onChange={e => this.onChange(e)} />
              </div>
            </form>
            <div className='submit'>
              <a
                className='edit-submit'
                onClick={this.props.editState.isFetching ? null : () => this.props.createUser(this.state.user)}
                >
                {this.props.editState.isFetching ? <Loader /> : 'Create'}
              </a>
            </div>
          </div>
        }

      </div>
    )
  }
}

export default NewUserForm
