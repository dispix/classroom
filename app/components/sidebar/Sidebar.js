import React from 'react'

import Button from '../button/Button'

import './Sidebar.scss'

/**
 *  Siderbar component
 */
class Sidebar extends React.Component {
  static get propTypes () {
    return {
      classes: React.PropTypes.string
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      isOpen: false
    }
  }

  /**
   *  Open or close the sidebar
   *  @method  toggle
   */
  toggle () {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render () {
    return (
      <div className='sidebar-container'>
        <aside
          className={`sidebar ${this.props.classes} ${this.state.isOpen ? 'isOpen' : 'isClosed'}`}
        >
          {this.state.isOpen ? null
            : <Button
              onClick={() => this.toggle()}
              classes='toggle-sidebar'
            >
              <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                <path d='M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z' />
              </svg>
            </Button>
          }
          <div className='sidebar-content'>
            {this.props.children}
          </div>
        </aside>
        {!this.state.isOpen ? null
          : <div
            className='sidebar-background'
            onClick={() => this.toggle()}
          />
        }
      </div>
    )
  }
}

export default Sidebar
