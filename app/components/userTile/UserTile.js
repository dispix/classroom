import React from 'react'
import { Link } from 'react-router'

import './UserTile.scss'

const UserTile = ({ user }) =>
  <Link to={`/users/${user.id}`} className='user-tile'>
    <h3>{`${user.first_name} ${user.last_name}`}</h3>
    <br />
    <p>{user.email}</p>
  </Link>

UserTile.propTypes = {
  user: React.PropTypes.object.isRequired
}

export default UserTile
