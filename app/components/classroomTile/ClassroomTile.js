import React from 'react'

import './ClassroomTile.scss'

const ClassroomTile = ({ classroom }) =>
  <div className='classroom-tile'>
    <h3>{classroom.subject}</h3>
  </div>

ClassroomTile.propTypes = {
  classroom: React.PropTypes.object.isRequired
}

export default ClassroomTile
