import React from 'react'
import { Link } from 'react-router'

import Loader from '../loader/Loader'

import './UserInfos.scss'

const UserInfos = ({ user, isFetchingClassrooms }) =>
  <div className='user-infos'>
    <h2>{`${user.first_name} ${user.last_name}`}</h2>
    <div className='infos'>
      <div className='info-block'>
        <p className='label'>Last used IP</p>
        <p>{user.ip_address}</p>
      </div>
      <div className='info-block'>
        <p className='label'>Email address:</p>
        <p><a href={`mailto:${user.email}`}>{user.email}</a></p>
      </div>
      <div className='classrooms-list'>
        <p className='label'>Registered courses</p>
        {isFetchingClassrooms &&
          <Loader />
        }
        {!isFetchingClassrooms &&
          <ul>
            {user.classrooms.map(classroom =>
              <li key={classroom.id}>
                <Link to={`/classes`} className='classroom'>
                  {classroom.subject}
                </Link>
              </li>
            )}
          </ul>
        }
      </div>
    </div>
    <Link
      to={`/users/${user.id}/edit`}
      className='edit-button'
      >
        Edit
    </Link>
  </div>

UserInfos.propTypes = {
  user: React.PropTypes.object.isRequired,
  isFetchingClassrooms: React.PropTypes.bool.isRequired
}

export default UserInfos
